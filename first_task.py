from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import padding as symmetric_padding
from cryptography.hazmat.primitives.ciphers import algorithms, modes
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
import os

# Function to generate RSA key pair
def generate_keypair():
    private_key = rsa.generate_private_key(
        public_exponent=65537,  # Commonly used public exponent
        key_size=2048  # Size of the key in bits
    )
    public_key = private_key.public_key()
    return private_key, public_key

# Function to encrypt a file with RSA and AES
def encrypt_file(file_name, public_key):
    with open(file_name, 'rb') as f:
        data = f.read()

    # Generating a 256-bit symmetric key for AES
    symmetric_key = os.urandom(32)  
    aes_cipher = Cipher(algorithms.AES(symmetric_key), modes.GCM())
    encryptor = aes_cipher.encryptor()
    ciphertext = encryptor.update(data) + encryptor.finalize()  # Encrypting the data
    tag = encryptor.tag  # Obtaining the authentication tag

    # Encrypting the symmetric key with the RSA public key
    encrypted_symmetric_key = public_key.encrypt(
        symmetric_key,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )

    # Writing the encrypted data to a file
    with open(file_name + '.enc', 'wb') as f:
        f.write(encrypted_symmetric_key)
        f.write(encryptor.nonce)
        f.write(tag)
        f.write(ciphertext)

# Function to decrypt a file with RSA and AES
def decrypt_file(file_name, private_key):
    with open(file_name, 'rb') as f:
        encrypted_symmetric_key = f.read(private_key.key_size // 8)  # Reading the encrypted symmetric key
        nonce = f.read(16)  # Reading the nonce
        tag = f.read(16)  # Reading the authentication tag
        ciphertext = f.read()  # Reading the encrypted data

    # Decrypting the symmetric key with the RSA private key
    symmetric_key = private_key.decrypt(
        encrypted_symmetric_key,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )

    # Decrypting the file content using the symmetric key
    aes_cipher = Cipher(algorithms.AES(symmetric_key), modes.GCM(nonce, tag))
    decryptor = aes_cipher.decryptor()
    decrypted_data = decryptor.update(ciphertext) + decryptor.finalize()

    # Writing the decrypted data to a file
    with open(file_name[:-4], 'wb') as f:
        f.write(decrypted_data)

# Generating keypair
private_key, public_key = generate_keypair()

# Example usage for encryption
file_to_encrypt = 'path/to/your_file.txt'
encrypt_file(file_to_encrypt, public_key)

# Example usage for decryption
file_to_decrypt = 'path/to/your_file.txt.enc'
decrypt_file(file_to_decrypt, private_key)
